export const snakeToCamel = (value: string): string => value.replace(/(\_\w)/g, (w) => w[1].toUpperCase());
