import { Knex } from "knex";

import { GenericObject } from "./types";

const ignoreFields = [
  "id", //
  "guid",
  "dateCreation",
  "dateChanges",
  "dateDeleted",
  "usrAccCreationId",
  "usrAccChangesId",
];

export abstract class Model {
  protected knex: Knex;
  protected id?: string;
  protected tableName: string;

  abstract columns: { [key: string]: string };

  constructor(knex: Knex, tableName: string) {
    this.knex = knex;
    this.tableName = tableName;
  }

  private extractFieldsToObj(acceptColumns?: string[]): GenericObject {
    const obj: GenericObject = {};

    Object.keys(this.columns).forEach((column) => {
      if (ignoreFields.includes(column)) {
        return;
      }

      // Если acceptColumns не передан, либо передан массив нулевой длины, функция его игнорирует.
      // Если же в массиве есть элементы, функция пропустит только те поля, которые есть в массиве.
      if (acceptColumns && acceptColumns.length && !acceptColumns.includes(column)) {
        return;
      }

      const dbColumn = this.columns[column];
      obj[dbColumn] = (this as GenericObject)[column];
    });

    return obj;
  }

  async insert({ usrAccCreationId }: { usrAccCreationId: string }) {
    const obj = this.extractFieldsToObj();

    obj["usr_acc_creation_id"] = usrAccCreationId;
    obj["usr_acc_changes_id"] = usrAccCreationId;

    await this.knex(this.tableName).insert(obj);
  }

  async update({ usrAccChangesId, columns }: { usrAccChangesId: string; columns: string[] }) {
    const obj = this.extractFieldsToObj(columns);

    obj["usr_acc_changes_id"] = usrAccChangesId;
    obj["date_changes"] = this.knex.raw("timezone('utc'::text, now())");

    await this.knex("farm").where("id", "=", this.id!).update(obj);
  }

  async delete({ usrAccChangesId }: { usrAccChangesId: string }) {
    await this.knex(this.tableName)
      .where("id", "=", this.id!)
      .update({
        usr_acc_changes_id: usrAccChangesId,
        date_changes: this.knex.raw("timezone('utc'::text, now())"),
        date_deleted: this.knex.raw("timezone('utc'::text, now())"),
      });
  }

  fromJSON(json: GenericObject): void {
    Object.keys(this.columns).forEach((column) => {
      if (!json[column]) {
        return;
      }

      (this as GenericObject)[column] = json[column];
    });
  }
}
