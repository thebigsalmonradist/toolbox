import { GenericObject } from "../db/types";

const traversalObject = (source: GenericObject, properties: GenericObject, parent: GenericObject) => {
  Object.keys(properties).forEach((propertyName) => {
    if (properties[propertyName].bypass) {
      parent[propertyName] = source[propertyName];

      return;
    }

    if (properties[propertyName].type === "object") {
      parent[propertyName] = {};

      traversalObject(source[propertyName], properties[propertyName].properties, parent[propertyName]);

      return;
    }

    if (properties[propertyName].type === "array") {
      const container: any[] = [];

      traversalArray(source[propertyName], properties[propertyName].items, container);

      parent[propertyName] = container;

      return;
    }

    parent[propertyName] = source[propertyName];
  });
};

const traversalArray = (source: any[], items: GenericObject, container: any[]) => {
  for (let i = 0; i < source.length; i++) {
    switch (items.type) {
      case "object":
        const obj = {};

        traversalObject(source[i], items.properties, obj);

        container[i] = obj;

        break;
      case "array":
        // TODO проверить этот кейс, когда он встретится на практике.
        const subContainer: any[] = [];

        traversalArray(source[i], items.items, subContainer);

        container[i] = subContainer;

        break;
      default:
        container[i] = source[i];
    }
  }
};

export const generateObject = (source: GenericObject, schema: GenericObject): GenericObject => {
  const result: GenericObject = {};

  traversalObject(source, schema.properties, result);

  return result;
};
